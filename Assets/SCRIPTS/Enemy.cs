﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{

    private Transform moveVertical;
	private Vector3 positionActual;

	public Transform ballPos;

	public float vely;
	private float movimiento;

	     
	void Update()
	{
        moveVertical = transform;

        positionActual = moveVertical.position;

        if (ballPos.position.y > transform.position.y) {
            movimiento = 1;
		} else {
            movimiento = -1;
		}
        movimiento *= Time.deltaTime;
        movimiento *= vely;
		transform.Translate(0, movimiento, 0);

		if (transform.position.y< -8.3f) {
			transform.position = new Vector3(transform.position.x, -8.3f, transform.position.z);
		}else if(transform.position.y > 8.3f) {
			transform.position = new Vector3(transform.position.x, 8.3f, transform.position.z);
		}
	}
}