﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy2 : MonoBehaviour
{
    

    private Transform moveVertical;  
    private Vector3 positionActual;

    public Transform ballPos;

    public float vely;
    private float movimiento;


    void Update()
    {
        movimiento *= Time.deltaTime;
        movimiento *= vely;
        transform.Translate(movimiento, 0, 0);

        moveVertical = transform;
        positionActual = moveVertical.position;

        if (ballPos.position.x > transform.position.x)
        {
            movimiento = 1;
        }
        else
        {
            movimiento = -1;
        }
       

        if (transform.position.x < -17.3f)   
        {
            transform.position = new Vector3(transform.position.y, -17.3f, transform.position.z);
        }
        else if (transform.position.x > 17.3f)
        {
            transform.position = new Vector3(transform.position.y, 17.3f, transform.position.z);
        }
    }
}