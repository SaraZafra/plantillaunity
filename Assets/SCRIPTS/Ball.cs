﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ball : MonoBehaviour {

    public Vector2 speed;
    public float maxVel;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        transform.Translate(speed.x * Time.deltaTime,speed.y*Time.deltaTime, 0);
	}
    void OnTriggerEnter(Collider other)
    {
        speed.x *= 1.25f;
        speed.y *= 1.25f;
        if(speed.x > maxVel)
        {
            speed.x = maxVel;
        }

        if(speed.y > maxVel)
        {
            speed.y = maxVel;
        }

        if(speed.x <-maxVel)
        {
            speed.x =-maxVel;
        }

        if(speed.y < -maxVel)
        {
            speed.y = -maxVel;
        }


        switch(other.tag)
        {
            case "Player":
                speed.x = -speed.x;
                break;
            case "Updown":
                speed.y = -speed.y;
                break;
            case "Leftright":
                speed.x = -speed.x;
                break;
            case "Enemy":
                speed.x = -speed.x;
                break;
            case "Enemy2":
                speed.y = -speed.y;
                break;
        }
    }
}
